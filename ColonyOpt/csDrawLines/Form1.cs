﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace csDrawLines
{
    public partial class Form1 : Form
    {
        private List<PointF> points_;

        public Form1()
        {
            InitializeComponent();
        }

        public void PaintPoints(List<PointF> points)
        {
            points_ = points;
            panel1.Refresh();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            Pen p = new Pen(Color.Red);
            for(int i = 0; i < points_.Count() - 1; ++i)
            {
                e.Graphics.DrawLine(p, points_[i], points_[i+1]);        
            }           
        }
    }
}
