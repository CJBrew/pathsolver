﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace csDrawLines
{
    public interface IDrawer
    {
        void ShowForm();
        //void Draw(PointF[] points, int nPoints);
        void Draw(List<PointF> s);
    };
    
    public class Drawer:IDrawer
    {
        public Drawer()
        {
            form1 = new Form1();
        }

        public void ShowForm()
        {
            form1.Show();
            // form1.Activate();
        }

        //public void Draw(PointF[] points, int nPoints)
        public void Draw(List<PointF> pointsList)
        {
            //// System.Drawing.Graphics g = ;
            //List<PointF> pointsList = new List<PointF>();
            //for (int i = 0; i < nPoints; ++i)
            //{
            //    pointsList.Add(points[i]);
            //}
            
            form1.PaintPoints(pointsList);
        }

        private Form1 form1;
    }
}
