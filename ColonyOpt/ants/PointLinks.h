#pragma once 

#include "Links.h"
#include <vector>

struct PointLinks
{        
    Links GetLinks(int point) const;
    void AddPoint();
    void AddLink(int point, const Link& l);
private:
    void CheckPoint(size_t point) const;
    std::vector<Links> links_;
};

PointLinks GetPointLinks(const Points& p, const Links& links);
