#pragma once
#include "Links.h"
#include <iosfwd>
#include <boost\operators.hpp>

struct IterationResult;

struct FinalResult
{
    FinalResult(const IterationResult&, const Links&);
    FinalResult():totalLength_(0.0)
    {}

    Links linksUsed_;
    double totalLength_;
};

typedef std::vector<int> LinkNumbers;

struct IterationResult : boost::less_than_comparable1<IterationResult>
{
    IterationResult():atIteration_(0), totalLength_(0.0)
    {}

    IterationResult& IterationResult::operator=(const IterationResult& that);

    int atIteration_;
    LinkNumbers linksUsed_;
    double totalLength_;
};

struct NIterationResult : boost::less_than_comparable1<IterationResult>
{
    NIterationResult():atIteration_(0), totalLength_(0.0)
    {}

    int atIteration_;
    Links linksUsed_;
    double totalLength_;
};


std::ostream& operator<<(std::ostream&, const FinalResult&);
std::ostream& operator<<(std::ostream&, const IterationResult&);

bool operator<(const IterationResult& a, const IterationResult& b);

void UpdateResultPheromone(const IterationResult&, Links&, const double pheromoneAmount);
void PrintResult(std::ostream&, const IterationResult&, const Links&);
double ResultLength(const IterationResult&, const Links&);