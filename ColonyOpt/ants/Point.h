#pragma once

#include <vector>
#include <iosfwd>

struct Point
{
    Point(float x, float y):x_(x),
                              y_(y)
    {}

    float x_;
    float y_;
};

std::ostream& operator<<(std::ostream& os, const Point& p);

typedef std::vector<Point> Points;
