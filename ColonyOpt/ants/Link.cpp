#include "Link.h"
#include <iostream>
#include <cmath>
#include <functional>
#include <algorithm>
#include <functional>
#include "MyException.h"

using namespace std;
using namespace std::tr1;

ostream& operator<<(ostream& os, const Link& link)
{
    os << link.GetFrom() << " to " << link.GetTo() << " prob = " << link.GetProbability();
    return os;
}

bool LinkBetween(const Link& link, int from, int to)
{
    return (link.GetFrom() == from && link.GetTo() == to);
}

bool LinkHasPoint(const Link& link, int point)
{
    return (link.GetFrom() == point || link.GetTo() == point);
}

void AddPheromone(Link& link, double p)
{
    link.SetPheromone(link.GetPheromone() + p);
}

void EvaporatePheromone(Link& link, double ratio)
{
    if(ratio > 1.0) return;
    const double phe = link.GetPheromone();    
    link.SetPheromone(phe * (1.0 - ratio));
}

void CalcProb(Link& link, double phe, double dis)
{    
    const double prob = CalculateProbability(link, phe, dis);
    link.SetProbability(prob);
}

double CalculateProbability(const Link& link, double pheromoneFactor, double distanceFactor)
{    
    if(!link.IsEnabled())
    {
        return 0;
    }

    //double p = it->GetPheromone();
    double phero = 0.0;
    const double pheromone = link.GetPheromone();
    if(pheromone < 1e-6) 
    {

        // ?
        phero = 1.0;
    }
    else
    {        
        phero = pow(pheromone, pheromoneFactor);
    }

    if(link.GetDistance() < 1e-6)
    {
        // near enough to zero!
        throw MyException("link distance 0 or near 0! Div by zero!");
    }

    const double distance = link.GetDistance();
    double visibility = pow((1.0 / distance), distanceFactor);        
    return phero * visibility;
}

namespace
{
    void ClearLink(Link& link)
    {
        link.SetPheromone(0.0);
        link.SetProbability(0.0);
    }
}

bool operator<(const Link& lhs, const Link& rhs)
{
    if(lhs.GetFrom() == rhs.GetFrom())
    {
        return (lhs.GetTo() < rhs.GetTo());
    }
    else
    {
        return (lhs.GetFrom() < rhs.GetFrom());
    }
}

bool SortLinks(const Link& lhs, const Link& rhs)
{
    return lhs.GetProbability() < rhs.GetProbability();
}
