#include "Point.h"
#include <iostream>

using namespace std;

std::ostream& operator<<(std::ostream& os, const Point& p)
{
    os << p.x_ << " : " << p.y_;
    return os;
}
