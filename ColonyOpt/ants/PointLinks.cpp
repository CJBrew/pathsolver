#include "PointLinks.h"
#include "Test.h"

void PointLinks::CheckPoint(size_t point) const
{
    Assert(point < links_.size(), "point > Links.Size!\n");
}

Links PointLinks::GetLinks(int point) const
{            
    CheckPoint(point);
    return links_[point];
}

void PointLinks::AddPoint()
{
    Links lps;
    links_.push_back(lps);
}

void PointLinks::AddLink(int point, const Link& l)
{
    CheckPoint(point);           
    links_[point].push_back(l);
}


PointLinks GetPointLinks(const Points& p, const Links& links)
{
    PointLinks plps;
    for(size_t i = 0; i < p.size(); ++i)
    {
        plps.AddPoint();
    }

    Links::const_iterator itEnd = links.end();
    for(Links::const_iterator it = links.begin() ; it != itEnd; ++it)
    {
        plps.AddLink(it->GetFrom(), *it);
    }
    return plps;
}
