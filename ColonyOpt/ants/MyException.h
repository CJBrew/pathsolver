#pragma once

#include <string>

class MyException : public std::exception
{
public:
	MyException() throw() {}
	~MyException() throw() {}
	MyException(const MyException& rhs) throw() : msg(rhs.msg) 
	{}
	
	MyException(const char * Message) : msg(Message) 
	{}

	MyException(const std::string & Message) : msg(Message) 
	{}

	MyException& operator=(const MyException& rhs) throw()
	{
		 msg = rhs.msg; 
	}

	const char * what() const throw()
	{
		return msg.c_str(); 
	}
private:
	std::string msg;

};
