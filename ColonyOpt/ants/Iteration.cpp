#include "Iteration.h"

#include <iostream>
#include <ctime>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <random>
#include <functional>

#include "Results.h"
#include "LinkProbability.h"
#include "Links.h"
#include "PointLinks.h"
#include "PointsSet.h"
#include "MyException.h"

using namespace std;
using namespace std::tr1;
using namespace std::tr1::placeholders;

namespace
{
    void SelectPossibleLinks(Links& links, const PointsSet& availablePoints)
    {
        for(Links::iterator it = links.begin(); it != links.end(); ++it)
        {   
            // if the destination of a link isn't in the set of available points
            // we can't use that link, so disable it
            it->SetEnabled(availablePoints.count(it->GetTo()) > 0);            
        }    
    }

    int FindNextPoint(const int fromPoint, PointLinks& pointLinks, 
        const PointsSet& availablePoints,
        mt19937& engine,
        const double pheromoneFactor, const double distanceFactor)
    {      
        // does a copy
        Links& linksFromHere = pointLinks.GetLinks(fromPoint);

        SelectPossibleLinks(linksFromHere, availablePoints);        

        for_each(linksFromHere.begin(), linksFromHere.end(), 
            bind(&CalcProb, _1, pheromoneFactor, distanceFactor));
        NormaliseProbabilities(linksFromHere);

        return DestinationOfRandomLink(engine, linksFromHere);
    }

    void StoreResult(IterationResult& result, const Links& links, const int fromPoint, const int toPoint)
    {
        const Link& chosenLink = FindLink(links, fromPoint, toPoint);
        result.linksUsed_.push_back(chosenLink.GetNumber());
    }
}

IterationResult RunIteration(const size_t iteration, ostream& oss, 
                             Links& allLinks, 
                             PointsSet availablePoints,
                             PointLinks& pointLinks,
                             const double pheromoneFactor, 
                             const double distanceFactor, 
                             const double pheromoneEvapRate, 
                             mt19937& engine,
                             const double maxLen)
{
    IterationResult result;
    for_each(allLinks.begin(), allLinks.end(), bind(&Link::SetEnabled, _1, true));
    for_each(allLinks.begin(), allLinks.end(), bind(&EvaporatePheromone, _1, pheromoneEvapRate));

    const size_t nPoints = availablePoints.size();

    const int startingPoint = 0; //uni(engine);
    int currentPoint = startingPoint;
    oss << "it:" << setw(6) << iteration << " rt: " << setw(3) << currentPoint;        
    availablePoints.erase(currentPoint);

    size_t count = 1;
    while(!availablePoints.empty())
    {
        const int nextPoint = FindNextPoint(currentPoint, pointLinks, availablePoints, 
            engine, 
            pheromoneFactor, distanceFactor);

        StoreResult(result, allLinks, currentPoint, nextPoint);

        oss << setw(3) << " -> " <<  nextPoint;

        currentPoint = nextPoint;
        availablePoints.erase(currentPoint);        

        ++count;
        if(count > nPoints)
        {          
            PrintPointsSet(cout, availablePoints);
            cout << oss.rdbuf();
            throw MyException("Too many points!");
        }
    }

    if(result.linksUsed_.size() != nPoints - 1)
    {
        throw MyException("Not all the points seem to have been used!");
    }


    Links::iterator finalLink = find_if(allLinks.begin(), allLinks.end(), 
        bind(&LinkBetween, _1, currentPoint, startingPoint));

    if(finalLink == allLinks.end())
    {
        throw MyException("Major fuck-up -- no final link found!");
    }

    //Link final(*finalLink);
    result.linksUsed_.push_back(finalLink->GetNumber());

    // Get Length
    double totalLength = ResultLength(result, allLinks);

    // Get Pheromone amount
    if(totalLength == 0)
    {	
        throw MyException("length == 0 in AddPheromone!");
    }
    oss  << "\n total Length = " << setw(8) << totalLength << endl;	

    result.atIteration_ = iteration;
    result.totalLength_ = totalLength;

    double pheromoneAmount = pow((maxLen / result.totalLength_), 1.0);
    UpdateResultPheromone(result, allLinks, pheromoneAmount);

    return result;
}

IterationResult FindBestPath(int nIterations, const int nIterationsIncrement,
                             const Links& links,
                             const PointsSet& pointsSet,
                             const PointLinks& pointLinks,
                             const double pheromoneFactor, 
                             const double distanceFactor, 
                             const double pheromoneEvapRate,
                             std::tr1::mt19937& engine, 
                             const double maxLen)
{
    Links localLinks;
    copy(links.begin(), links.end(), back_inserter(localLinks));

    PointLinks localPointLinks = pointLinks;

    bool bestFound = false;
    IterationResult bestResult;

    //engine.seed(static_cast<unsigned int>(time(0)));

    for(int i = 0; i < nIterations; ++i)
    {
        ostringstream oss;

        IterationResult result = RunIteration(i, oss, 
            localLinks, pointsSet, localPointLinks,
            pheromoneFactor, distanceFactor, pheromoneEvapRate, 
            engine, maxLen);          

        if(!bestFound || result < bestResult)
        {
            if(result.linksUsed_.size() != pointsSet.size())
            {
                throw MyException("Not all the points seem to have been used!");
            }

            bestResult = result;
            bestFound = true;
            oss  << "At iteration " << i << " new best or equal best path \n";
            PrintResult(oss, result, links);
            oss << "\n";

            nIterations += nIterationsIncrement;
            // cout << oss.str() << endl;
        }        
    }

    return bestResult;
}
