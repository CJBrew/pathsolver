#pragma once 
#include <iosfwd>
#include "Results.h"
#include "Links.h"
#include "PointsSet.h"
#include "PointLinks.h"
#include <random>

IterationResult RunIteration(const size_t iteration, std::ostream& oss, 
                             Links& allLinks, 
                             PointsSet availablePoints,
                             PointLinks& pointLinks,
                             const double pheromoneFactor, 
                             const double distanceFactor, 
                             const double pheromoneEvapRate, 
                             std::tr1::mt19937& engine,
                             const double maxLen);

IterationResult FindBestPath(const int nIterations, const int nIterationsIncrement,
                             const Links& links,
                             const PointsSet& pointsSet,
                             const PointLinks& pointLinks,
                             const double pheromoneFactor, 
                             const double distanceFactor, 
                             const double pheromoneEvapRate,
                             std::tr1::mt19937& engine, 
                             const double maxLen);