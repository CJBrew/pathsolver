#include "Test.h"	
#include "MyException.h"	
#include <cmath>
#include <algorithm>
#include <ctime>
#include "Link.h"
#include "LinkProbability.h"
#include "PointLinks.h"
#include "PointsSet.h"
#include "FloatingPoint.h"
#include <random>
#include <functional>
#include <iostream>

using namespace std;
using namespace std::tr1;
using namespace std::tr1::placeholders;

void Assert (bool b, const string & s) 	
{
    if (!b)

    {
        throw MyException (s.c_str ());
    }
}

namespace 	
{
    bool DoubleAlmostEqual (double a, double b, int sigFigs) 	
    {
        if (sigFigs == 0)
        {
            throw MyException ("Sig Figs can't equal 0 in DoubleTol!");
        }
        const double d = fabs (a - b);
        return (d<(1.0 / sigFigs));
    }

    void TestRandom () 	
    {
        mt19937 eng;
        eng.seed (static_cast<unsigned long >(time (0)));

        {
            uniform_real<double >unir;
            double d = unir (eng);
            Assert (d<1, "uniform real<1");
        } 	
        {
            /*CumulativeProbabilities cp;
            cp.push_back (0.2);
            cp.push_back (0.5);
            cp.push_back (1);
            CheckProbs (cp);
            const int trials = 100;
            for (int i = 0; i<trials; ++i)			
            {
                size_t res = RandomByIndex (eng, cp);
                Assert ((res >= 0 && res <= cp.size ()), "random by index wrong index!");
            } 	*/
        }
        {
           /* CumulativeProbabilities cp;
            cp.push_back (0.2);
            cp.push_back (0.5);
            cp.push_back (1);
            CheckProbs (cp);
            const int trials = 100000;
            vector<int >results (3, 0);
            for (int i = 0; i<trials; ++i)

            {
                size_t res = RandomByIndex (eng, cp);
                ++results[res];
            } 		int tot = results[0] + results[1] + results[2];

            vector<double > dResults;
            vector<int >::const_iterator itEnd = results.end ();

            for (vector<int >::const_iterator it = results.begin ();  it != itEnd; 	++it)			
            {
                dResults.push_back (static_cast<double> (*it) / tot);
            } 		Assert (DoubleAlmostEqual (dResults[0] / tot, 0.2, 2),
                "results 0!");
            Assert (DoubleAlmostEqual (dResults[1] / tot, 0.3, 2), "results 1!");
            Assert (DoubleAlmostEqual (dResults[2] / tot, 0.5, 2), "results 2!");
            Assert (tot == trials, "total results != trials!");*/
        } 
    } 	



    void TestCumulativeProbabilities () 	
    {
      /*  Probabilities ps;
        ps.push_back (0.2);
        ps.push_back (0.3);
        ps.push_back (0.5);
        CumulativeProbabilities cp = GetNormalisedCumulativeProbabilities (ps);
        CheckProbs (cp);
        Assert (EqualDoubles (cp[0], 0.2, 10), "cp 0");
        Assert (EqualDoubles (cp[1], 0.5, 10), "cp 1");
        Assert (EqualDoubles (cp[2], 1.0, 10), "cp 2");
        cp.push_back (1.4);
        try 	
        {
            CheckProbs (cp);
        } 
        catch (const exception & ex) 	
        {
            const string s (ex.what ());
            Assert (s == "Cumulative Probability > 1!", "cum prob fail!");
        }*/
    } 		

    void TestLinks2()
    {
        Points points;
        points.push_back (Point (0, 0));
        points.push_back (Point (3, 4));
        points.push_back (Point (8, 8));
        Links links = GetLinks (points);
        for_each(links.begin(), links.end(), std::tr1::bind(&Link::SetEnabled, _1, false));
        EnableLinksFrom(links, 0);
  
        Assert (links.size () == 6, "links size = 6!");
        Assert (links[0].GetFrom() == 0, "links[0] from!");
        Assert (links[0].GetTo() == 1, "links[0] to!");
        Assert (links[0].IsEnabled(),    "links[0] enabled!");
        Assert (links[1].GetFrom() == 0, "links[1] from!");
        Assert (links[1].GetTo() == 2, "links[1] to!");        
        Assert (links[1].IsEnabled(),    "links[1] enabled!");
        Assert (links[2].GetFrom() == 1, "links[2] from!");
        Assert (links[2].GetTo() == 0, "links[2] to!");
        Assert (!links[2].IsEnabled(),    "links[2] disabled!");
        Assert (links[3].GetFrom() == 1, "links[3] from!");
        Assert (links[3].GetTo() == 2, "links[3] to!");
        Assert (!links[3].IsEnabled(),    "links[3] disabled!");
        Assert (links[4].GetFrom() == 2, "links[4] from!");
        Assert (links[4].GetTo() == 0, "links[4] to!");        
        Assert (!links[4].IsEnabled(),    "links[4] disabled!");
        Assert (links[5].GetFrom() == 2, "links[5] from!");
        Assert (links[5].GetTo() == 1, "links[5] to!");
        Assert (!links[5].IsEnabled(),    "links[5] disabled!");
    }

    void TestDisableImpossible()
    {
        Points points;
        points.push_back (Point (0, 0));
        points.push_back (Point (3, 4));
        points.push_back (Point (8, 8));
        Links links = GetLinks (points);
        PointsSet pointsSet;
        pointsSet.insert(1);
        pointsSet.insert(2);
        DisableImpossibleLinks(links, pointsSet);
        
        const Links enabledLinks = GetEnabledLinks(links);
        Assert(enabledLinks.size() == 4, "4 enabled links!");
//        Links(Links& links, const PointsSet& pointsSet)
    }

    void TestLinks () 	
    {
        Points points;
        points.push_back (Point (0, 0));
        points.push_back (Point (3, 4));
        points.push_back (Point (8, 8));
        Links links = GetLinks (points);
        Assert (links.size () == 6, "links size = 6!");
        Assert (links[0].GetFrom() == 0, "links[0] from!");
        Assert (links[0].GetTo() == 1, "links[0] to!");
        Assert (DoubleAlmostEqual (links[0].GetDistance(), 5.0, 3),    "links[0] dist!");
        Assert (links[1].GetFrom() == 0, "links[1] from!");
        Assert (links[1].GetTo() == 2, "links[1] to!");        
        Assert (DoubleAlmostEqual (links[1].GetDistance(), 11.313, 3),	    "links[1] dist!");
        Assert (links[2].GetFrom() == 1, "links[2] from!");
        Assert (links[2].GetTo() == 0, "links[2] to!");
        Assert (DoubleAlmostEqual (links[2].GetDistance(), 5.0, 3),    "links[2] dist!");    
        Assert (links[3].GetFrom() == 1, "links[3] from!");
        Assert (links[3].GetTo() == 2, "links[3] to!");
        Assert (DoubleAlmostEqual (links[3].GetDistance(), 6.40, 3),	    "links[3] dist!");
        Assert (links[4].GetFrom() == 2, "links[4] from!");
        Assert (links[4].GetTo() == 0, "links[4] to!");        
        Assert (DoubleAlmostEqual (links[4].GetDistance(), 11.313, 3),	    "links[4] dist!");
        Assert (links[5].GetFrom() == 2, "links[5] from!");
        Assert (links[5].GetTo() == 1, "links[5] to!");
        Assert (DoubleAlmostEqual (links[5].GetDistance(), 6.40, 3),	    "links[5] dist!");
        Links::const_iterator found = find_if (links.begin (), links.end (), bind(&LinkBetween, _1, 0, 2));
        Assert (found != links.end (), "find_if links 1");
        Assert (found->GetFrom() == 0, "found from!");
        Assert (found->GetTo() == 2, "found to!");
        Assert (DoubleAlmostEqual (found->GetDistance(), 11.313, 3), "found dist!");
        Links::const_iterator found2 = find_if (links.begin (), links.end (), bind(&LinkBetween, _1, 5, 3));
        Assert (found2 == links.end (), " not found!");

        DisableLinks(links, 0);
        int count = 0;
        Links::const_iterator itEnd = links.end();
        for(Links::const_iterator it = links.begin(); it != itEnd; ++it)
        {
            if(it->IsEnabled()) 
            {
                ++count;
            }
        }
        Assert(count == 2, "links erased!");
    } 		

    void ClearPheromone (Link & l) 	
    {
        EvaporatePheromone(l, 1.0);
    } 			

    void TestLinkProbabilities () 	
    {
        Points points;
        points.push_back (Point (0, 0));
        points.push_back (Point (3, 4));
        points.push_back (Point (8, 8));
        Links links = GetLinks (points);
        for_each(links.begin(), links.end(), bind(&AddPheromone, _1, 1.0));
        for_each(links.begin(), links.end(), bind(&CalcProb, _1, 1.0, 1.0));
        Links lps;
        Links::iterator itEnd = links.end();
        for(Links::iterator it = links.begin(); it != itEnd; ++it)
        {
            lps.push_back(*it);
        }

        NormaliseProbabilities(lps);
        Assert(lps.size() == 6, " 6 lps!");
        Assert(DoubleAlmostEqual(lps[0].GetProbability(), 0.45, 2), "probability[0]");
        Assert(DoubleAlmostEqual(lps[2].GetProbability(), 0.65, 2), "probability[1]");
        Assert(DoubleAlmostEqual(lps[4].GetProbability(), 1.0, 5), "probability[2]");
    }

    void TestPheromones()
    {
        Points points;    
        points.push_back(Point(0, 0));
        points.push_back(Point(3, 4));
        points.push_back(Point(8, 8));
        Links links = GetLinks(points);
        for_each(links.begin(), links.end(), bind(&AddPheromone, _1, 1.0));

        const double rho = 0.5;
        for_each(links.begin(), links.end(), bind(&EvaporatePheromone, _1, rho));
        Assert(DoubleAlmostEqual(links[0].GetPheromone(), 0.5, 3),"link[0] phero");
        Assert(DoubleAlmostEqual(links[1].GetPheromone(), 0.5, 3),"link[1] phero");
        Assert(DoubleAlmostEqual(links[2].GetPheromone(), 0.5, 3),"link[2] phero");
        /*
        Pheromone Update

        ti,j = (1 - ?)ti,j + ?ti,j

        where

        ti,j is the amount of pheromone on a given edge i,j

        ? is the rate of pheromone evaporation

        and ?ti,j is the amount of pheromone deposited, typically given by

        ?ti,j = (ant k travelled on i,j) ?  1/Lk  :  0

        where Lk is the cost of the kth ant's tour (typically length).
        */
    }

    Points GetPoints()
    {
        Points points;
        points.push_back(Point(0,0));
        points.push_back(Point(3,4));
        points.push_back(Point(7,2));
        points.push_back(Point(8,10));
        points.push_back(Point(4.2f,20));
        points.push_back(Point(12,15));
        points.push_back(Point(18,8));
        points.push_back(Point(10,10));
        points.push_back(Point(21,1));
        points.push_back(Point(25,25));
        return points;
    }

    void TestPointLinks()
    {
        std::tr1::mt19937 engine;
        engine.seed(static_cast<unsigned long>(time(0)));

        const int iterations = 10;

        const Points points = GetPoints();
        Links links = GetLinks(points);
        const PointLinks pointLinks = GetPointLinks(points, links);
        
        for(unsigned int i = 0; i < points.size(); ++i)
        {        
            Assert(pointLinks.GetLinks(i).size() == (points.size() - 1), "point link ptrs size");      
        }
    }
} 		

void Test()
{
    TestRandom();
    TestCumulativeProbabilities();
    TestLinks();
        TestLinks2();
    TestLinkProbabilities();
    TestDisableImpossible();
    TestPheromones();
    //    TestAnt();
    TestPointLinks();
    cout << "Tests OK!\n";
}
