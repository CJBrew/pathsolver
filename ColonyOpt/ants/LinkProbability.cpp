#include "LinkProbability.h"
#include "MyException.h"
#include <algorithm>
#include <iostream>
#include <random>

using namespace std;
using namespace std::tr1;

namespace
{

    struct ProbGtThan
    {
        explicit ProbGtThan(double p):prob_(p)
        {}

        bool operator()(const Link& l)
        {
            return (l.GetProbability() > prob_);
        }

    private:
        double prob_;
    };


}

int DestinationOfRandomLink(mt19937& eng, const Links& links)
{    
    uniform_real<double> unir;
    double p = unir(eng);

    if(links.size() == 1)
    {
        return links.begin()->GetTo();
    }

    Links::const_iterator it = find_if(links.begin(), links.end(), ProbGtThan(p));
    
    if(it == links.end())
    {
        throw MyException("RandomByIndex didn't find an answer!");
    }

    return it->GetTo();       
}
