#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <ctime>
#include "Test.h"
#include "Results.h"
#include "Links.h"
#include "Point.h"
#include "PointLinks.h"
#include "Iteration.h"
#include "ACSIteration.h"
#include <random>
#include <functional>

#include "..\DrawRoute\DrawRouteImports.h"

using namespace std;
using namespace std::tr1;
using namespace std::tr1::placeholders;

namespace
{
    Points Randomize(const Points& points)
    {
        Points randomizedPoints;
        copy(points.begin(), points.end(), back_inserter(randomizedPoints));
        std::random_shuffle(randomizedPoints.begin(), randomizedPoints.end());
        return randomizedPoints;
    };

    Points GetQatarPoints()
    {
        Points points;
        ifstream ifp("../qatar194.txt");
        char buf[100];
        while(ifp.getline(buf, 100))
        {
            istringstream iss(buf);
            int di;
            float t1;
            float t2;
            iss >> di >> t1 >> t2;
            points.push_back(Point(t1, t2));
        };

        return points;
    }

    Points GetPoints()
    {
        Points points;
        points.push_back(Point(0,0));	
        points.push_back(Point(4,3));	
        points.push_back(Point(34,3));	
        points.push_back(Point(13,21));	
        points.push_back(Point(17,3));	
        points.push_back(Point(27,23));	
        points.push_back(Point(3,10));	
        points.push_back(Point(10,6));	
        points.push_back(Point(30,30));
        return points;
    }

    Points GetPoints2()
    {
        Points points;
        points.push_back(Point(0,0));	
        points.push_back(Point(4,3));	
        points.push_back(Point(34,3));	
        points.push_back(Point(13,21));	
        points.push_back(Point(17,3));	
        points.push_back(Point(27,23));	
        points.push_back(Point(3,10));	
        points.push_back(Point(10,6));	
        points.push_back(Point(30,30));
        points.push_back(Point(-10,30));	
        points.push_back(Point(-4,-3));	
        points.push_back(Point(-34,-3));	
        points.push_back(Point(-13,-21));	
        points.push_back(Point(-17,-3));	
        points.push_back(Point(-27,-23));	
        points.push_back(Point(-3,-10));	
        points.push_back(Point(-10,-6));	
        points.push_back(Point(-30,-30));
        return points;
    }


    double GetMaxLen(const Links& links, int nPoints)
    {
        double maxLen = 0.0;
        Links::const_iterator itEnd = links.end();
        for(Links::const_iterator it = links.begin(); it != itEnd; ++it)
        {
            if(it->GetDistance() > maxLen)
            {
                maxLen = it->GetDistance();
            }
        }
        return maxLen * nPoints;
    }

    FinalResult MainLoop(const Points& points, 
        const int n_runs, const int iterations, const int nIterationsIncrement, 
        const double pheromoneFactor,
        const double distanceFactor,
        const double pheromoneEvapRate,
        mt19937& engine)
    {     
        cout << "\n\n__PARAMETERS__ : " 
            << "Points size: " << points.size()
            << " n_runs = " << n_runs 
            << " iterations = " << iterations 
            << " iterationInc = " << nIterationsIncrement
            << " pheromoneFactor = " << pheromoneFactor
            << " distanceFactor = " << distanceFactor
            << " pheromoneEvapRate = " << pheromoneEvapRate << endl;


        const Links links = GetLinks(points);
        const PointLinks pointLinks = GetPointLinks(points, links);
        const PointsSet pointsSet = GetPointsSet(points);

        const double maxLen = GetMaxLen(links, points.size());

        bool bestOverallFound = false;
        IterationResult bestResultOverall;

        cout << "#points = " << points.size() << "\n";
        cout << "#links = " << links.size() << "\n";
        cout << "points size = " << points.size() << endl;   

        for(int i_base = 0; i_base < n_runs; ++i_base)
        {
            cout << "___RUN #" << (i_base + 1) << endl;

            // bool bestFound = false;
            IterationResult bestResult = FindBestPath(iterations, nIterationsIncrement,
                links, pointsSet, pointLinks, 
                pheromoneFactor, distanceFactor, pheromoneEvapRate,
                engine, maxLen);

            if(bestResult <= bestResultOverall || !bestOverallFound)
            {
                bestOverallFound = true;
                cout << "Found a new best path:\n";                
                PrintResult(cout, bestResult, links);
                bestResultOverall = bestResult;
            }        
        }
        cout << "Done.\n";

        FinalResult finalResult(bestResultOverall, links);
        
        return finalResult;
    }


    FinalResult MainLoopACS(const Points& points, 
        const int n_runs, const int n_ants,  const int iterationTolerance, 
        const double pheromoneFactor,
        const double distanceFactor,
        const double pheromoneEvapRate,
        mt19937& engine)
    {     
        cout << "\n\n__ACS PARAMETERS__ : " 
            << "Points size: " << points.size()
            //<< " n_runs = " << n_runs 
            << " iterations = " << iterationTolerance 
            << " n_ants = " << n_ants 
            << " pheromoneFactor = " << pheromoneFactor
            << " distanceFactor = " << distanceFactor
            << " pheromoneEvapRate = " << pheromoneEvapRate << endl;


        const Links links = GetLinks(points);
        const PointLinks pointLinks = GetPointLinks(points, links);
        const PointsSet pointsSet = GetPointsSet(points);

        const double maxLen = GetMaxLen(links, points.size());

        bool bestOverallFound = false;
        IterationResult bestResultOverall;

        bool updatePheromoneForEachAnt = true;
        bool iterationBestPheroUpdate = true;
        bool bestScorePheroUpdate = true;

        cout << "#points = " << points.size() << "\n";
        cout << "#links = " << links.size() << "\n";
        cout << "points size = " << points.size() << endl;   

        for(int i_base = 0; i_base < n_runs; ++i_base)
        {
            cout << "___RUN #" << (i_base + 1) << endl;

            // bool bestFound = false;
            IterationResult bestResult = ACSFindBestPath(n_ants, iterationTolerance, 
                points, links, pointsSet, pointLinks, 
                pheromoneFactor, distanceFactor, pheromoneEvapRate,
                engine, maxLen, 
                updatePheromoneForEachAnt, iterationBestPheroUpdate, bestScorePheroUpdate);

            if(bestResult <= bestResultOverall || !bestOverallFound)
            {
                bestOverallFound = true;
                cout << "Found a new best path:\n";
                PrintResult(cout, bestResult, links);
                bestResultOverall = bestResult;
            }        
        }
        cout << "Done.\n";

        FinalResult finalResult(bestResultOverall, links);
        return finalResult;
    }
}

int main()
{
    // http://www.scholarpedia.org/article/Ant_colony_optimization
    try
    {
        DrawRoute::OpenWindow();

        Test();

        mt19937	engine;    
        engine.seed(static_cast<unsigned int>(time(0)));        

        const double pheromoneFactor = 0.3;
        const double distanceFactor = 1;
        const double pheromoneEvapRate = 0.1;

        for(int i = 0; i < 5; ++i)
        {
            const int nRuns = 1;
            const int antsPerIteration = 10;
            const int maxIterationsWithNoChange = 20000;
              
            FinalResult result = MainLoopACS(GetPoints(), 
                nRuns, antsPerIteration, maxIterationsWithNoChange,
                pheromoneFactor ,  distanceFactor ,
                //pheromoneFactor / (pow(2.0, static_cast<double>(i))),
                //distanceFactor * (pow(2.0, static_cast<double>(i))), 
                pheromoneEvapRate, // * (pow(2.0, static_cast<double>(i))),
                engine);

            cout << result;
        }        
    }
    catch(const exception& ex)
    {
        cout << "Caught exception: " << ex.what() << endl;
    }
    cout << "Press enter...";
    string s;
    getline(cin, s);
    return 0;
}

