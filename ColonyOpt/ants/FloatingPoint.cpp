#include "FloatingPoint.h"
#include <cassert>
#include <limits>
#include <cmath>

using namespace std;

// Bruce Dawson's function AlmostEqual2sComplement, to compare floats
// http://www.cygnus-software.com/papers/comparingfloats/Comparing%20floating%20point%20numbers.htm
bool EqualFloats(float a, float b, int tolerance)
{
    // Make sure maxUlps is non-negative and small enough that the
    // default NAN won't compare as equal to anything.
    assert(tolerance > 0 && tolerance < 4 * 1024 * 1024);

    const float maxFloat = numeric_limits<float>::max();
    const int maxFloatAsInt = *reinterpret_cast<const int*>(&maxFloat);
    
    int aInt = *reinterpret_cast<int*>(&a);
    // Make aInt lexicographically ordered as a twos-complement int        
    if (aInt < 0)
    {
        aInt =  maxFloatAsInt - aInt;
    }

    int bInt = *reinterpret_cast<const int*>(&b);
    // Make bInt lexicographically ordered as a twos-complement int
    if (bInt < 0)
    {
        bInt = maxFloatAsInt - bInt;
    }

    int intDiff = aInt - bInt;
    intDiff = intDiff > 0 ? intDiff : -intDiff;   
    if(intDiff <= tolerance)
    {
        return true;
    }
    return false;
}

// Bruce Dawson's function modified for doubles
// http://www.cygnus-software.com/papers/comparingfloats/Comparing%20floating%20point%20numbers.htm
bool EqualDoubles(double a, double b, long long tolerance)
{
    // Make sure maxUlps is non-negative and small enough that the
    // default NAN won't compare as equal to anything.
    assert(tolerance > 0 && tolerance < 8 * 1024 * 1024);

    const double maxDouble = numeric_limits<double>::max();
    const long long maxDoubleAsLong = *reinterpret_cast<const long long*>(&maxDouble);
    
    long long aInt = *reinterpret_cast<long long*>(&a);
    // Make aInt lexicographically ordered as a twos-complement int        
    if (aInt < 0)
    {
        aInt = maxDoubleAsLong - aInt;
    }

    long long bInt = *reinterpret_cast<long long*>(&b);
    // Make bInt lexicographically ordered as a twos-complement int
    if (bInt < 0)
    {
        bInt = maxDoubleAsLong - bInt;
    }

    // abs implementation
    long long intDiff = aInt - bInt;
    if(intDiff < 0)
    {
        intDiff *= -1;
    }

    if(intDiff <= tolerance)
    {
        return true;
    }
    return false;
}
