#include "ACSIteration.h"
#include "..\DrawRoute\DrawingData.h"
#include "..\DrawRoute\DrawRouteImports.h"

#include <iostream>
#include <ctime>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <random>
#include <functional>

#include "Results.h"
#include "LinkProbability.h"
#include "Links.h"
#include "PointLinks.h"
#include "PointsSet.h"
#include "MyException.h"

using namespace std;
using namespace std::tr1;
using namespace std::tr1::placeholders;

namespace
{   
    void PostData(const Points& points, const Links& allLinks, const IterationResult& ir)
    {
        DrawingData dd;
        
        const Point& f = points.at(allLinks[*ir.linksUsed_.begin()].GetFrom());
        
        dd.Add(DrawingData::Point(f.x_, f.y_));
        for(LinkNumbers::const_iterator it = ir.linksUsed_.begin(); it != ir.linksUsed_.end(); ++it)
        {
            const Point& t = points.at(allLinks[*it].GetTo());
            dd.Add(DrawingData::Point(t.x_, t.y_));
        }

        DrawRoute::PostNewData(dd);
    }
    
    double ScalePheromoneAmount(double maxLen, double len)
    {   
        if(len < 1e-6) return 0.0;

        double ratio = maxLen / len;     
        double p = pow(ratio , 20.0 );        
        double scaledDown = p / 10000.0;              
        return scaledDown;
    };

    void SelectPossibleLinks(Links& links, const PointsSet& availablePoints)
    {
        for(Links::iterator it = links.begin(); it != links.end(); ++it)
        {   
            // if the destination of a link isn't in the set of available points
            // we can't use that link, so disable it
            it->SetEnabled(availablePoints.count(it->GetTo()) > 0);            
        }    
    }

    int FindNextPoint(const int fromPoint, PointLinks& pointLinks, 
        const PointsSet& availablePoints,
        mt19937& engine,
        const double pheromoneFactor, const double distanceFactor)
    {      
        // does a copy
        Links& linksFromHere = pointLinks.GetLinks(fromPoint);

        SelectPossibleLinks(linksFromHere, availablePoints);        

        for_each(linksFromHere.begin(), linksFromHere.end(), 
            bind(&CalcProb, _1, pheromoneFactor, distanceFactor));
        NormaliseProbabilities(linksFromHere);

        return DestinationOfRandomLink(engine, linksFromHere);
    }

    void StoreResult(IterationResult& result, Links& links, 
                    const int fromPoint, const int toPoint,
                    const double pheromoneDecayCoefficient = 0.005)
    {
        Link& chosenLink = FindLink(links, fromPoint, toPoint);
        
        /*** Local pheromone update for ACS ***/        
        EvaporatePheromone(chosenLink, pheromoneDecayCoefficient);
        /***/

        result.linksUsed_.push_back(chosenLink.GetNumber());
    }

    IterationResult RunACSIteration(ostream& oss, 
        Links& allLinks, 
        PointsSet availablePoints,
        PointLinks& pointLinks,
        const double pheromoneFactor, 
        const double distanceFactor, 
        mt19937& engine,
        const double maxLen,
        bool updatePheromoneForEachAnt)
    {
        IterationResult result;
        for_each(allLinks.begin(), allLinks.end(), bind(&Link::SetEnabled, _1, true));

        const size_t nPoints = availablePoints.size();

        uniform_int<int> unii;
        const int startingPoint = unii(engine, nPoints); 

        int currentPoint = startingPoint;
        oss << " rt: " << setw(3) << currentPoint;        
        availablePoints.erase(currentPoint);

        size_t count = 1;
        while(!availablePoints.empty())
        {           
            const int nextPoint = FindNextPoint(currentPoint, pointLinks, availablePoints, 
                engine, 
                pheromoneFactor, distanceFactor);

            StoreResult(result, allLinks, currentPoint, nextPoint);

            oss << setw(3) << " -> " <<  nextPoint;

            currentPoint = nextPoint;
            availablePoints.erase(currentPoint);        

            ++count;
            if(count > nPoints)
            {          
                PrintPointsSet(cout, availablePoints);
                cout << oss.rdbuf();
                throw MyException("Too many points!");
            }
        }

        if(result.linksUsed_.size() != nPoints - 1)
        {
            throw MyException("Not all the points seem to have been used!");
        }

        Links::iterator finalLink = find_if(allLinks.begin(), allLinks.end(), 
            bind(&LinkBetween, _1, currentPoint, startingPoint));

        if(finalLink == allLinks.end())
        {
            throw MyException("Major fuck-up -- no final link found!");
        }

        //Link final(*finalLink);
        result.linksUsed_.push_back(finalLink->GetNumber());

        // Get Length
        double totalLength = ResultLength(result, allLinks);
        result.totalLength_ = totalLength;

        /* Optionally, update pheromone for each ant */
        if(updatePheromoneForEachAnt)
        {
            double pheromoneAmount = ScalePheromoneAmount(maxLen, result.totalLength_); 

            UpdateResultPheromone(result, allLinks, pheromoneAmount);
        }

        return result;
    }
}


typedef vector<IterationResult> IterationResults;

IterationResult RunACSIterations(int nACSIterations,
                             Links& links,
                             const PointsSet& pointsSet,
                             const PointLinks& pointLinks,
                             const double pheromoneFactor, 
                             const double distanceFactor, 
                             const double pheromoneEvapRate,
                             std::tr1::mt19937& engine, const double maxLen,
                             bool updatePheromoneForEachAntEachIteration,
                             bool iterationBest,
                             bool bestScore)
{    
    PointLinks localPointLinks = pointLinks;

    IterationResults iterations;

    for(int i = 0; i < nACSIterations; ++i)
    {
        ostringstream oss;

        iterations.push_back(RunACSIteration(oss, 
            links, pointsSet, localPointLinks,
            pheromoneFactor, distanceFactor, 
            engine, maxLen, 
            updatePheromoneForEachAntEachIteration));   
    }

    IterationResults::const_iterator itBestResult = max_element(iterations.begin(), iterations.end());
    IterationResult best = *itBestResult;
    return best;
}

IterationResult ACSFindBestPath(const size_t nAntsEachIteration, const int iterationTolerance,                                
                                const Points& points,
                             const Links& links,
                             const PointsSet& pointsSet,
                             const PointLinks& pointLinks,
                             const double pheromoneFactor, 
                             const double distanceFactor, 
                             const double pheromoneEvapRate,
                             std::tr1::mt19937& engine, 
                             const double maxLen,
                             bool updatePheromoneForEachAntEachIteration,
                             bool iterationBest,
                             bool bestScore)
{
    Links localLinks;
    copy(links.begin(), links.end(), back_inserter(localLinks));

    bool bestFound = false;
    IterationResult bestYet;
        
    int iterCount = 0;
    int iterationsSinceLastBest = 0;

    while(++iterationsSinceLastBest < iterationTolerance)
    {                         
        /*if(iterCount % 100 == 0)
        {
           PrintLinksPheromone(cout, localLinks);
        }*/

        IterationResult result = RunACSIterations(nAntsEachIteration, localLinks, pointsSet, pointLinks,
                        pheromoneFactor, distanceFactor, pheromoneEvapRate,
                        engine, maxLen,
                        updatePheromoneForEachAntEachIteration, iterationBest, bestScore);
        
        result.atIteration_ = ++iterCount;

        // after every iteration, update the pheromone for the best ant
        if(iterationBest)
        {
            double pheromoneAmount = ScalePheromoneAmount(maxLen, result.totalLength_);

            UpdateResultPheromone(result, localLinks, pheromoneAmount);            
        }
        if(bestScore)
        {
            double pheromoneAmount = ScalePheromoneAmount(maxLen, result.totalLength_); 

            UpdateResultPheromone(bestYet, localLinks, pheromoneAmount);            
        }

        if(result < bestYet || !bestFound)
        {
            iterationsSinceLastBest = 0;
            bestFound = true;
            bestYet = result;            
            cout << "Best! ";
            PrintResult(cout, bestYet, localLinks);
            cout << endl;

            PostData(points, links, result);                    
        }
    }       

    return bestYet;
}