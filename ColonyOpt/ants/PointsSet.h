#pragma once

#pragma warning(push)
#pragma warning(disable:4396)
#include <boost/unordered_set.hpp>
#pragma warning(pop)
#include <iosfwd>
#include "Point.h"
#include "Links.h"

typedef boost::unordered_set<int> PointsSet;

PointsSet GetPointsSet(const Points&);
void PrintPointsSet(std::ostream& os, const PointsSet&);
void DisableImpossibleLinks(Links& links, const PointsSet& pointsSet);