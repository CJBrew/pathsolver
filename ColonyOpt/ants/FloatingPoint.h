#pragma once

bool EqualFloats(float a, float b, int tolerance);
bool EqualDoubles(double a, double b, long long tolerance);
