#pragma once

#include <vector>
#include "Link.h"

typedef std::vector<Link> Links;

Links GetLinks(const Points&);
void PrintLinksPheromone(std::ostream&, const Links&);
void PrintLinks(std::ostream&, const Links&);
Links GetPointLinks(const Links& lps, int point);
void ClearLinks(Links& links);
void DisableLinks(Links& links, int point);
void EnableLinksFrom(Links& links, int point);
void NormaliseProbabilities(Links& Links);
Links GetEnabledLinks(const Links&);

Link FindLink(const Links& links, int from, int to);