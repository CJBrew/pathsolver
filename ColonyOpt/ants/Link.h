#pragma once

#include <iosfwd>
#include "Point.h"

struct Link
{
    friend std::ostream& operator<<(std::ostream&, const Link&);
    
    Link(int number, int f, int t, double d):linkNumber_(number),
                                 from_(f), 
                                 to_(t), 
                                 distance_(d),
                                 pheromone_(0),
                                 probability_(0),
				 enabled_(true)
    {}

    int GetNumber() const { return linkNumber_; }
    void SetNumber(int i) { linkNumber_ = i; }
    int GetFrom() const { return from_; }
    int GetTo() const { return to_; } 
    double GetDistance() const { return distance_; }
    double GetPheromone() const { return pheromone_; }
    double GetProbability() const { return probability_; }
    bool IsEnabled() const { return enabled_; }    
	
    void SetProbability(double p) { probability_ = p; }    
    void SetPheromone(double p) { pheromone_ = p; }
    void SetEnabled(bool enabled) { enabled_ = enabled; }

private:

    int linkNumber_;
    int from_;
    int to_;
    double distance_;
    double pheromone_;
    double probability_;
    bool enabled_;
};

void AddPheromone(Link&, double p);
void EvaporatePheromone(Link&, double ratio);    
double CalculateProbability(const Link& link, double pheromoneFactor, double distanceFactor);

void CalcProb(Link& link, double phe, double dis);

bool operator<(const Link&, const Link&);

bool LinkBetween(const Link& link, int from, int to);
bool LinkHasPoint(const Link& link, int point);
