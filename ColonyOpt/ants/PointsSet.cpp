#include "PointsSet.h"
#include <iostream>

using namespace std;

PointsSet GetPointsSet(const Points& points)
{
    PointsSet set;
    Points::const_iterator itEnd = points.end();
    int i = 0;
    for(Points::const_iterator it = points.begin(); it != itEnd; ++it, ++i)
    {
        set.insert(i);	
    }
    return set;
}

void PrintPointsSet(ostream& os, const PointsSet& pointsSet)
{
    PointsSet::const_iterator pEnd = pointsSet.end();
    for(PointsSet::const_iterator pit = pointsSet.begin(); pit != pEnd; ++pit)
    {	
        os << *pit << " ";
    }
    os << endl;
}

void DisableImpossibleLinks(Links& links, const PointsSet& pointsSet)
{
    Links::iterator itEnd = links.end();
    for(Links::iterator it = links.begin(); it != itEnd; ++it)
    {        
        if(pointsSet.end() == find(pointsSet.begin(), pointsSet.end(), it->GetTo()))
        {
            it->SetEnabled(false);
        }
    }
}
