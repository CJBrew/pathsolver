#include "Links.h"
#include "copy_if.h"
#include <iostream>
#include <cmath>
#include <functional>
#include <algorithm>
#include <functional>
#include "Link.h"
#include "PointsSet.h"
#include "MyException.h"

using namespace std;
//using namespace std::tr1;
//using namespace std::tr1::placeholders;

namespace
{
    double GetHypotenuse(const Point& coord1, const Point& coord2)
    {
        double x = coord2.x_ - coord1.x_;
        double y = coord2.y_ - coord1.y_;
        double d = sqrt(x*x + y*y);
        if(d < 0.1)
        {
            cout << " points " << coord1 << " " << coord2 << " have near-zero distance!" << endl;
        }
        return d;
    }
}

void RenumberLinks(Links& links)
{
    for(unsigned int i = 0; i < links.size(); ++i)
    {
        links[i].SetNumber(i);
    }
}

Links GetLinks(const Points& points)
{
    const int pointsSize = points.size();
    Links links;
    links.reserve(pointsSize * (pointsSize - 1));

    int count = 0;
    for(int i = 0 ; i < pointsSize; ++i)
    {
        for(int j = i + 1 ; j < pointsSize; ++j)
        {
            const double distance = GetHypotenuse(points[i], points[j]);
            links.push_back(Link(-1, i, j, distance));   
            links.push_back(Link(-1, j, i, distance));   
            if(distance < 0.1)
            {
                throw MyException("link distance < 0.1!");
            }
        }
    }    

    std::sort(links.begin(), links.end()/*, &SortLinks*/);

    RenumberLinks(links);

    return links;
}

void PrintLinksPheromone(ostream& os, const Links& ls)
{
    Links::const_iterator itEnd = ls.end();
    for(Links::const_iterator it = ls.begin(); it != itEnd; ++it)
    {
        os << it->GetFrom() << " -> " << it->GetTo() << " : " << it->GetPheromone() << "\n";
    }
}

void PrintLinks(ostream& os, const Links& ls)
{
    Links::const_iterator itEnd = ls.end();
    for(Links::const_iterator it = ls.begin(); it != itEnd; ++it)
    {
        os << *it << endl;
    }
}

namespace
{
    bool LinkFrom(const Link& link, int from)
    {
        return (link.GetFrom() == from);
    }
}

Links GetPointLinks(const Links& lps, int point)
{
    Links lpset;
    lpset.reserve(lps.size());
    Links::const_iterator itEnd = lps.end();
    for(Links::const_iterator it = lps.begin(); it != itEnd; ++it)
    {
        if(it->IsEnabled())
        {
            if(LinkFrom(*it, point))
            {
                lpset.push_back(*it);
            }
        }
    }
    return lpset;
}

namespace 
{
    void Clear(Link& link)
    {
        link.SetProbability(0);
        link.SetPheromone(0);
    }
}

void ClearLinks(Links& links)
{
    for_each(links.begin(), links.end(), bind(&Clear, placeholders::_1));
}

namespace
{
    void EnableIfFrom(Link& link, int point)
    {
        if(LinkFrom(link, point))
        {
            link.SetEnabled(true);
        }
        else
        {
            link.SetEnabled(false);
        }
    }
}

void EnableLinksFrom(Links& links, int point)
{
    for_each(links.begin(), links.end(), bind(&EnableIfFrom, placeholders::_1, point)); 
}

namespace
{
    void DisableIf(Link& link, int point)
    {
        if(LinkHasPoint(link, point))
        {
            link.SetEnabled(false);
        }
    }
}

void DisableLinks(Links& links, int point)
{
    for_each(links.begin(), links.end(), bind(&DisableIf, std::placeholders::_1, point)); 
}

namespace
{
    void NormaliseProbability(Link& link, double total)
    {
        double p = link.GetProbability();
        link.SetProbability(p / total);
    }

    bool SortLinks(const Link& lhs, const Link& rhs)
    {
        return lhs.GetProbability() > rhs.GetProbability();
    }
}

void NormaliseProbabilities(Links& links)
{
    if(links.empty())
    {
        throw MyException("No valid links!");
    }

    //sort(links.begin(), links.end(), SortLinks);

    double total = 0;
    Links::iterator itEnd = links.end();
    for(Links::iterator it = links.begin(); it != itEnd; ++it)
    {        
        double p = it->GetProbability();
        it->SetProbability(p + total);
        total += p;        
    }
    for_each(links.begin(), links.end(), bind(&NormaliseProbability, placeholders::_1, total));    
}

namespace
{
    bool LinkIsEnabled(const Link& l)
    {
        return l.IsEnabled();
    }
}

Links GetEnabledLinks(const Links& linksIn)
{
    Links links;
    copy_if(linksIn.begin(), linksIn.end(), back_inserter(links), LinkIsEnabled);
    return links;
}


namespace
{
    struct IsLinkFromTo
    {
        IsLinkFromTo(int from, int to):from_(from), to_(to)
        {}

        bool operator()(const Link& link)
        {
            return link.GetFrom() == from_ && link.GetTo() == to_;
        }
 
    private:
        int from_;
        int to_;
    };
}

Link FindLink(const Links& links, int from, int to)
{
    Links::const_iterator it = find_if(links.begin(), links.end(), IsLinkFromTo(from, to));
    if(it == links.end())
    {
        throw MyException("Unable to find the link!");
    }
    return *it;
}