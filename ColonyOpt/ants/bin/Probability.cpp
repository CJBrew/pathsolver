#include "Probability.h"
#include "Link.h"
#include <algorithm>
#include "MyException.h"

using namespace std;
using namespace boost;

typedef double Probability;
typedef vector<Probability> Probabilities;
typedef vector<Probability> CumulativeProbabilities;
/*
bool IsGt1(Probability d)
{
    return d > 1;
}

CumulativeProbabilities GetNormalisedCumulativeProbabilities(const Probabilities& probs)
{
    CumulativeProbabilities cps;
    Probabilities::const_iterator itEnd = probs.end();
    Probability total = 0;
    for(Probabilities::const_iterator it = probs.begin(); it != itEnd; ++it)
    {        
        cps.push_back(*it + total);
        total += *it;
    }
    
    CumulativeProbabilities::const_iterator itEnd2 = cps.end();
    for(CumulativeProbabilities::iterator it = cps.begin(); it != itEnd2; ++it)
    {
        *it /= total;
    }
    return cps;
}

void CheckProbs(const CumulativeProbabilities& cumulativeProbabilities)
{
    CumulativeProbabilities::const_iterator it = find_if(cumulativeProbabilities.begin(), cumulativeProbabilities.end(), IsGt1);
    if(it != cumulativeProbabilities.end())
    {
        throw MyException("Cumulative Probability > 1!");
    }
}*/

//int RandomByIndex(mt19937& eng, const CumulativeProbabilities& cumulativeProbabilities)
//{    
//    uniform_real<Probability> unir;
//    Probability p = unir(eng);
//
//    const int cpSize = cumulativeProbabilities.size();
//    for(int i = 0; i <= cpSize; ++i)
//    {           
//        if(p <= cumulativeProbabilities[i])
//        {
//            return i;
//        }
//    }
//    throw MyException("RandomByIndex didn't find an answer!");
//}
