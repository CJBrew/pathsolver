#pragma once

#include <vector>
//#include "Random.h"

struct Point
{
    Point(double x, double y):x_(x),
                              y_(y)
    {}

    double x_;
    double y_;
};

typedef std::vector<Point> Points;

struct Link
{
    Link(int number, int f, int t, double d):linkNumber_(number),
                                 from_(f), 
                                 to_(t), 
                                 distance_(d),
                                 pheromone_(0)
    {}

    int linkNumber_;
    int from_;
    int to_;
    double distance_;
    double pheromone_;
};

typedef std::vector<Link> Links;

Links GetLinks(const Points&);
