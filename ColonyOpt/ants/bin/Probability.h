#pragma once 

#include <boost/random.hpp>
#include <vector>

#include "LinkProbability.h"

typedef double Probability;
typedef std::vector<Probability> Probabilities;
typedef std::vector<Probability> CumulativeProbabilities;

bool IsGt1(Probability);
CumulativeProbabilities GetNormalisedCumulativeProbabilities(const Probabilities&);
void CheckProbs(const CumulativeProbabilities&);
int RandomByIndex(boost::mt19937& eng, const CumulativeProbabilities&);
