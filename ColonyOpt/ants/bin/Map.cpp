#include "map.h"
#include "linkProbability.h"
#include <cmath>

using namespace std;

namespace
{
    double GetHypotenuse(const Point& coord1, const Point& coord2)
    {
        double x = coord2.x_ - coord1.x_;
        double y = coord2.y_ - coord1.y_;
        return sqrt(x*x + y*y);
    }
}

Links GetLinks(const Points& points)
{
    const int pointsSize = points.size();
    Links links;
    links.reserve(pointsSize * (pointsSize - 1));
    
    int count = 0;
    for(int i = 0 ; i < pointsSize; ++i)
    {
        for(int j = i + 1 ; j < pointsSize; ++j, ++count)
        {
            links.push_back(Link(count, i, j, GetHypotenuse(points[i], points[j])));   
        }
    }    
    return links;
}

LinkProbabilities GetLinkProbabilities(const Links& links)
{
    LinkProbabilities linkProbs;
    
    const double alpha = 0.5;
    const double beta = 0.5;

    Links::const_iterator itEnd = links.end();
    for(Links::const_iterator it = links.begin(); it != itEnd; ++it)
    {      
        double phero = pow(it->pheromone_, alpha);
        if(it->distance_ == 0)
        {
            throw std::exception("Zero distance in link!");
        }
        double desire = pow((1 / it->distance_), beta);
        linkProbs.push_back(LinkProbability(it->linkNumber_, phero * desire));
    }
        // probs.push_back(
// p from i to j = (pheromone between i&j)^a * (desirability of path (1/distance)^b) / TOTAL(all p)
    

    return linkProbs;
}
