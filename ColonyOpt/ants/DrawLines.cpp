class DrawerWrapper
{
public:
    DrawerWrapper()
    {
         drawer_ = gcnew Drawer();
         points_ = gcnew List<PointF>();
    }

private:
    Drawer^ drawer_;
    List<PointF>^ points_;
};

//    
//    drawer->ShowForm();
//
//    List<PointF>^ points = gcnew List<PointF>();
//    
//    points->Add(PointF(10,20));
//    points->Add(PointF(20,40));
//    points->Add(PointF(26,40));
//    points->Add(PointF(20,10));
//    points->Add(PointF(10,28));
//    
//    drawer->Draw(points);
//    
////    string s;
////    getline(cin, s);
//    return 1;