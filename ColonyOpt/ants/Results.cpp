#include "Results.h"
#include <iostream>
#include <algorithm>

using namespace std;

ostream& operator<<(ostream& os, const FinalResult& result)
{
    os << "Result length: " << result.totalLength_ << " path: ";
    Links::const_iterator it = result.linksUsed_.begin();
    Links::const_iterator itEnd = result.linksUsed_.end();
    for(; it != itEnd - 1; ++it)
    {
        os << (*it).GetFrom() << " -> ";
    }
    os << (*it).GetTo() << endl;
    return os;
}

//
//ostream& operator<<(ostream& os, const IterationResult& result)
//{
//    os << "Result at Iteration " << result.atIteration_ << " length: " << result.totalLength_ << " path: ";
//    LinkNumbers::const_iterator it = result.linksUsed_.begin();
//    LinkNumbers::const_iterator itEnd = result.linksUsed_.end();
//    for(; it != itEnd - 1; ++it)
//    {
//        os << *it << " -> ";
//    }
//    os << *it << endl;
//    return os;
//}


IterationResult& IterationResult::operator=(const IterationResult& that)
{
    if(this != &that)
    {
        atIteration_ = that.atIteration_;
        linksUsed_.clear();
        copy(that.linksUsed_.begin(), that.linksUsed_.end(), back_inserter(linksUsed_));
        totalLength_ = that.totalLength_;
    }
    return *this;
}

void PrintResult(ostream& os, const IterationResult& result, const Links& links)
{
    os << "Result at Iteration " << result.atIteration_ << " length: " << result.totalLength_ << " path: ";
    LinkNumbers::const_iterator it = result.linksUsed_.begin();
    LinkNumbers::const_iterator itEnd = result.linksUsed_.end();
    for(; it != itEnd - 1; ++it)
    {
        os << links.at(*it).GetFrom() << " -> ";
    }
    os << links.at(*it).GetTo() << endl;
    //return os;
}

bool operator<(const IterationResult& a, const IterationResult& b)
{
    return a.totalLength_ < b.totalLength_;
}

void UpdateResultPheromone(const IterationResult& result, Links& links, const double pheromoneAmount)
{
    LinkNumbers::const_iterator itEnd = result.linksUsed_.end();
    for(LinkNumbers::const_iterator it = result.linksUsed_.begin(); it != itEnd; ++it)
    {
        AddPheromone(links.at(*it), pheromoneAmount);                        
    }
}

double ResultLength(const IterationResult& result, const Links& links)
{
    double totalLength = 0;
    LinkNumbers::const_iterator itEnd = result.linksUsed_.end();
    for(LinkNumbers::const_iterator it = result.linksUsed_.begin(); it != itEnd; ++it)
    {
        totalLength += links.at(*it).GetDistance();
    }
    return totalLength;
}

FinalResult::FinalResult(const IterationResult& ir, const Links& links)
{
    totalLength_ = ir.totalLength_;

    const LinkNumbers::const_iterator itEnd = ir.linksUsed_.end();
    for(LinkNumbers::const_iterator it = ir.linksUsed_.begin(); it != itEnd; ++it)
    {
        linksUsed_.push_back(links.at(*it));
    }
}