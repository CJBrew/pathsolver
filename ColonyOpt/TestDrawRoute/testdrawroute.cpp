#include <boost\thread.hpp>
#include <random>
#include "..\DrawRoute\DrawRouteImports.h"
#include "..\DrawRoute\DrawingData.h"

using namespace std::tr1;
using namespace boost;

void Post(const DrawingData& dd)
{
    DrawRoute::PostNewData(dd);
}

int main()
{
    DrawRoute::OpenWindow();
    std::tr1::mt19937 mt;
    mt.seed();
    std::tr1::uniform_int<int> ui(0, 100);
    
    for(int i = 0 ; ; ++i)
    {
        DrawingData dd;
        dd.Add(DrawingData::Point(ui(mt), ui(mt)));
        dd.Add(DrawingData::Point(ui(mt), ui(mt)));
        dd.Add(DrawingData::Point(ui(mt), ui(mt)));
        Post(dd);
        this_thread::sleep(boost::posix_time::seconds(5));
    }    
}