#include <iostream>
#include <vector>
#include <string>

using namespace std;
using namespace System;
using namespace System::Collections::Generic;
using namespace System::Drawing;
using namespace csDrawLines;

int main()
{
    Drawer^ drawer = gcnew Drawer();
    
    drawer->ShowForm();

    List<PointF>^ points = gcnew List<PointF>();
    
    points->Add(PointF(10,20));
    points->Add(PointF(20,40));
    points->Add(PointF(26,40));
    points->Add(PointF(20,10));
    points->Add(PointF(10,28));
    
    drawer->Draw(points);
    
//    string s;
//    getline(cin, s);
    return 1;
}
