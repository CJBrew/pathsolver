#include <iostream>
#include <vector>
#include <string>

//#import "..\csDrawLines\bin\Debug\csDrawLines.tlb" raw_interfaces_only

using namespace std;
using namespace System;
using namespace System::Collections::Generic;
using namespace System::Drawing;
using namespace csDrawLines;

//class IDrawer
//{
//    virtual ~IDrawer()
//    {}
//
//    virtual void Draw(const std::vector<std::pair<float, float>>&) = 0;
//};

//class DotNetDrawer 
//{
//public:
//    DotNetDrawer()
//    {
//        points_ = gcnew List<PointF>();
//        drawer_ = gcnew Drawer();  
//    }
//
//    void Draw(const std::vector<std::pair<float, float>>& points)
//    {
//
//    }
//
//    List<PointF>^ points_;
//    Drawer^ drawer_;
//};
//
//class __declspec(dllexport) LineDrawer
//{
//public:
//    LineDrawer()
//    {      
//        //drawer_->ShowForm();
//    }
//
//    void Update(const std::vector<std::pair<float, float>>& points)
//    {
//        // copy to points_
//        // update drawer_
//    //    pDrawer_->Draw(points);
//    }
//private:
//    // IDrawer* pDrawer_;    
//};

struct CPointF
{
    float x;
    float y;
};

int main()
{
    Drawer^ drawer = gcnew Drawer();
    
    drawer->ShowForm();

    List<PointF>^ points = gcnew List<PointF>();
    
    points->Add(PointF(10,20));
    points->Add(PointF(20,40));
    points->Add(PointF(26,40));
    points->Add(PointF(20,10));
    points->Add(PointF(10,28));
    
    /* CPointF* pPoints = new CPointF[points->Count];

    for(int i = 0; i < points->Count; ++i)
    {
        pPoints[i].x = points[i].X;
        pPoints[i].y = points[i].Y;
    }*/

   // cli::array<PointF, 5>^ pointsArray;// = gcnew cli::array<PointF, 5>();
    
    //drawer->Draw(pointsArray, points->Count);
    drawer->Draw(points);
    
    string s;
    getline(cin, s);
    return 1;
}

//int main()
//{
//    // Initialize COM.
//    HRESULT hr = CoInitialize(NULL);
//
//    // Create the interface pointer.
//    IDrawerPtr pIdrawer(__uuidof(Drawer));
//
//    long lResult = 0;
//
//    // Call the Add method.
//    //pIdrawer->ShowForm();
//
//    // wprintf(L"The result is %d", lResult);
//
//    // Uninitialize COM.
//    CoUninitialize();
//    return 0;
//}