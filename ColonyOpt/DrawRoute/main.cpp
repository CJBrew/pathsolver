#include <iostream> 
#include <windows.h> 
#include <gdiplus.h>
#include <vector>
#include <algorithm>
#include <functional>
#include <boost\thread.hpp> 
#include "ScopeGuard.h"

#include "DrawingData.h"

using namespace std;
using namespace Gdiplus;

static bool newData = false;
static DrawingData currentData;
static HWND hWindow;
static size_t winWidth;
static size_t winHeight;
static HINSTANCE g_inst;
static RECT g_rect;
static ULONG_PTR gdiplusToken;

namespace
{
    bool CompX(const DrawingData::Point& lhs, const DrawingData::Point& rhs)
    {
        return lhs.x_ < rhs.x_;
    }

    bool CompY(const DrawingData::Point& lhs, const DrawingData::Point& rhs)
    {
        return lhs.y_ < rhs.y_;
    }

    vector<PointF> GetScaledVertices(const DrawingData& dd)
    {       
        const float maxX = max_element(dd.begin(), dd.end(), CompX)->x_;        
        const float maxY = max_element(dd.begin(), dd.end(), CompY)->y_;        
        const float minX = min_element(dd.begin(), dd.end(), CompX)->x_;        
        const float minY = min_element(dd.begin(), dd.end(), CompY)->y_;        

        const size_t borderSize = 5;
        
        const size_t width = g_rect.right - g_rect.left - 2*borderSize;
        const size_t height = g_rect.bottom - g_rect.top - 2*borderSize;

        const float widthScale = (maxX - minX) / width;
        const float heightScale = (maxY - minY) / height;
        
        const float Xcorrection = 0 - minX + 0;
        const float Ycorrection = 0 - minY + 0;

        // 0,0 is top left
        vector<PointF> vertices;

        const int nPoints = currentData.size();
        for(int i = 0; i < nPoints - 1; ++i)
        {               
            const DrawingData::Point f = currentData.at(i);

            PointF vertex;
            vertex.X = ((f.x_ + Xcorrection) / widthScale) + borderSize;
            vertex.Y = ((f.y_ + Ycorrection) / heightScale) + borderSize;
            
            vertices.push_back(vertex);
        }

        return vertices;
    }

    LRESULT CALLBACK WindowProcedure
        (HWND hwnd, unsigned int message, WPARAM wParam, LPARAM lParam)
    {
        switch (message)
        {
        case WM_DESTROY:
            ::PostQuitMessage (0);
            return 0;
        case WM_SIZE:
           GetClientRect(hWindow, &g_rect);
           break;
        case WM_PAINT:
            if(newData)
            {
                PAINTSTRUCT ps;
               
                HDC hdc = BeginPaint(hwnd, &ps);
                //ScopeGuard endPaint = MakeGuard(hdc, bind(EndPaint, _1, &ps));

                Graphics graphics(hdc);
                const Pen blackPen(Gdiplus::Color::Black, 1);
                const Pen redPen(Gdiplus::Color::Red, 1);

                const float xSize = 1;

                const vector<PointF>& vertices = GetScaledVertices(currentData);
                const int nPoints = vertices.size();
                for(int i = 0; i < nPoints;)
                {
                    const PointF& f = vertices.at(i);
                    const PointF& t = vertices.at(++i < nPoints ? i : 0);
                    
                    graphics.DrawLine(&redPen, f, t);
                    
                    const PointF f1(f.X + xSize, f.Y + xSize);
                    const PointF f2(f.X - xSize, f.Y - xSize);
                    const PointF f3(f.X - xSize, f.Y + xSize);
                    const PointF f4(f.X + xSize, f.Y - xSize);
                    graphics.DrawLine(&blackPen, f1, f2);
                    graphics.DrawLine(&blackPen, f3, f4);                                        
                }

                EndPaint(hwnd, &ps); 
                
            }
            break;
        }
    
        return ::DefWindowProc (hwnd, message, wParam, lParam );
    }
}

class WinClass
{
public:
    WinClass (WNDPROC winProc, wchar_t const * className, HINSTANCE hInst);
    void Register ()
    {
        ::RegisterClass (&_class);
    }
private:
    WNDCLASS _class;
};

WinClass::WinClass
(WNDPROC winProc, wchar_t const * className, HINSTANCE hInst)
{
    _class.style = 0;
    _class.lpfnWndProc = winProc; // window procedure: mandatory
    _class.cbClsExtra = 0;
    _class.cbWndExtra = 0;
    _class.hInstance = hInst;         // owner of the class: mandatory
    _class.hIcon = 0;
    _class.hCursor = ::LoadCursor (0, IDC_ARROW); // optional
    _class.hbrBackground = (HBRUSH) (COLOR_WINDOW + 1); // optional
    _class.lpszMenuName = 0;
    _class.lpszClassName = className; // mandatory
}

class WinMaker
{
public:
    WinMaker (): _hwnd (0) {}
    WinMaker (wchar_t const * caption, 
        wchar_t const * className,
        HINSTANCE hInstance);

    HWND GetHandle() const { return _hwnd; }

    void Show (int cmdShow)
    {
        ::ShowWindow (_hwnd, cmdShow);
        ::UpdateWindow (_hwnd);
    }
protected:
    HWND _hwnd;
};

WinMaker::WinMaker (wchar_t const * caption, 
                    wchar_t const * className,
                    HINSTANCE hInstance)
{
    _hwnd = ::CreateWindow (
        className,            // name of a registered window class
        caption,              // window caption
        WS_OVERLAPPEDWINDOW,  // window style
        CW_USEDEFAULT,        // x position
        CW_USEDEFAULT,        // y position
        120,        // width
        120,        // height
        0,                    // handle to parent window
        0,                    // handle to menu
        hInstance,            // application instance
        0);                   // window creation data
}
const wchar_t g_szClassName[] = L"LineDrawingWindow";


namespace DrawRoute
{
    extern "C" __declspec(dllexport)
        void PostNewData(const DrawingData& dd)
    {
        currentData.Clear();

        for(DrawingData::points_const_iterator it = dd.begin(); it != dd.end(); ++it)
        {
            currentData.Add(*it);
        }

        newData = true;
        InvalidateRect(hWindow, &g_rect, 1);
        SendMessage(hWindow, WM_PAINT, 0, 0);
        //DoPaint(hWindow);
    }
}

int DoOpenWindow(HINSTANCE hInstance, int nShow)
{
    wchar_t className[] = L"MyWindow";
    WinClass winClass(WindowProcedure, className, hInstance);
    winClass.Register();

    WinMaker win(L"", className, hInstance);
    win.Show(nShow);

    hWindow = win.GetHandle();
    GetClientRect(hWindow, &g_rect);

    MSG  msg;
        int status;
        while ((status = ::GetMessage (&msg, 0, 0, 0)) != 0)
        {
            if (status == -1)
                return -1;
            ::TranslateMessage (&msg);
            ::DispatchMessage (&msg);
        }

    return msg.wParam;
    //return 1;
}

struct MessageLoop
{
    MessageLoop(HINSTANCE hInstance, int nShow):hInstance_(hInstance), nShow_(nShow)
    {}
    
    void operator()()
    {
        DoOpenWindow(hInstance_, nShow_);
    }

private:
HINSTANCE hInstance_;
int nShow_;
};


extern "C" __declspec(dllexport)
void OpenWindow()
{    
    GdiplusStartupInput gdiplusStartupInput;
    GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
    
    MessageLoop ml(g_inst, 1);
    boost::thread newThread(ml);
}

BOOL WINAPI DllMain(HINSTANCE hinstDLL,  // handle to DLL module
                    DWORD fdwReason,     // reason for calling function
                    LPVOID lpvReserved)  // reserved                    
{
    switch(fdwReason)
    {      
    case DLL_PROCESS_ATTACH:
        g_inst = hinstDLL;
        // Perform any necessary cleanup.
        break;

    case DLL_THREAD_ATTACH:   
        g_inst = hinstDLL;        

        break;

    case DLL_THREAD_DETACH:
        // Do thread-specific cleanup.
        break;

    case DLL_PROCESS_DETACH:
        GdiplusShutdown(gdiplusToken);

        // Perform any necessary cleanup.
        break;
    }

    return TRUE;  // Successful DLL_PROCESS_ATTACH.
}
