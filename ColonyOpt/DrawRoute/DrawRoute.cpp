#include <windows.h> 
#include <gdiplus.h>
#include <vector>
#include <functional>
#include "ScopeGuard.h"

using namespace std;
using namespace std::tr1;
using namespace std::tr1::placeholders;
using namespace Gdiplus;


extern "C" __declspec(dllexport)
void UpdateDrawing(const vector<pair<float, float>>& points)
{

}

//namespace
//{
    const wchar_t g_szClassName[] = L"LineDrawingWindow";

    LRESULT WINAPI WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
    {
        switch(msg)
        {
        case WM_DESTROY:
            PostQuitMessage(0);
            return(0);
        case WM_PAINT:
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hwnd, &ps);
            //ScopeGuard endPaint = MakeGuard(hdc, bind(EndPaint, _1, &ps));

            Graphics graphics(hdc);

            // Draw the line
            Pen pen(Gdiplus::Color::Red, 1);
            graphics.DrawLine(&pen, 10, 10, 20, 20);

            EndPaint(hwnd, &ps); 
            return(0);
        }

        return(DefWindowProc(hwnd, msg, wParam, lParam));
    }
//}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, 
                   LPSTR, int iCmdShow)
{
    GdiplusStartupInput gdiplusStartupInput;

    ULONG_PTR gdiplusToken;
    GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
    ScopeGuard gdiPlusGuard = MakeGuard(GdiplusShutdown, gdiplusToken);

    WNDCLASSEX wc;
    //Step 1: Registering the Window Class
    wc.cbSize        = sizeof(WNDCLASSEX);
    wc.style         = 0;
    wc.lpfnWndProc   = WndProc;
    wc.cbClsExtra    = 0;
    wc.cbWndExtra    = 0;
    wc.hInstance     = hInstance;
    wc.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
    wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
    wc.lpszMenuName  = NULL;
    wc.lpszClassName = g_szClassName;
    wc.hIconSm       = LoadIcon(NULL, IDI_APPLICATION);

    if(!RegisterClassEx(&wc))
    {
        MessageBox(NULL, L"Window Registration Failed!", L"Error!",
            MB_ICONEXCLAMATION | MB_OK);
        return 0;
    }

    // Step 2: Creating the Window
    HWND hWnd = CreateWindowEx(WS_EX_CLIENTEDGE,
        g_szClassName,
        L"Drawing",
        WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT, CW_USEDEFAULT, 240, 240,
        NULL, NULL, hInstance, NULL);

    if(hWnd == NULL)
    {
        MessageBox(NULL, L"Window Creation Failed!", L"Error!",
            MB_ICONEXCLAMATION | MB_OK);
        return 0;
    }

    ShowWindow(hWnd, iCmdShow);
    UpdateWindow(hWnd);

    MSG msg;
    while(GetMessage(&msg, NULL, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    // GdiplusShutdown(gdiplusToken);
    return msg.wParam;
}