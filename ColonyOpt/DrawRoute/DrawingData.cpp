#include "DrawingData.h"

void DrawingData::Add(const DrawingData::Point& p)
{
    points_.push_back(p);
}

void DrawingData::Clear()
{
    points_.clear();
}
