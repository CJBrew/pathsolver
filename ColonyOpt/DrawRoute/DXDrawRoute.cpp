#include <windows.h> 
#include <d3d9.h> 

// globals
LPDIRECT3D9       g_pDirect3D = NULL;
LPDIRECT3DDEVICE9 g_pDirect3D_Device = NULL;

struct D3DVertex
{
   float x;
   float y;
   float z;
   float rhw;
   DWORD color;
};


LRESULT WINAPI WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam); 

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPSTR lpCmdLine, 

                   int nShow)
{
    MSG msg;

    WNDCLASSEX wc = {sizeof(WNDCLASSEX), CS_VREDRAW|CS_HREDRAW|CS_OWNDC, 
        WndProc, 0, 0, hInstance, NULL, NULL, (HBRUSH)(COLOR_WINDOW+1), 
        NULL, L"DX9_TUTORIAL1_CLASS", NULL}; 

    RegisterClassEx(&wc);

    HWND hMainWnd = CreateWindow(L"DX9_TUTORIAL1_CLASS", 
        L"DirectX 9 Bare Bones Tutorial 1", 
        WS_OVERLAPPEDWINDOW, 100, 100, 300, 300, 
        NULL, NULL, hInstance, NULL);

    g_pDirect3D = Direct3DCreate9(D3D_SDK_VERSION);

    D3DPRESENT_PARAMETERS PresentParams;
    memset(&PresentParams, 0, sizeof(D3DPRESENT_PARAMETERS));

    PresentParams.Windowed = TRUE;
    PresentParams.SwapEffect = D3DSWAPEFFECT_DISCARD;

    g_pDirect3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hMainWnd, 
        D3DCREATE_SOFTWARE_VERTEXPROCESSING, &PresentParams,
        &g_pDirect3D_Device);

    ShowWindow(hMainWnd, nShow);
    UpdateWindow(hMainWnd);

    while(GetMessage(&msg, NULL, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return(0);
} 



LRESULT WINAPI WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    switch(msg)
    {
    case WM_DESTROY:
        PostQuitMessage(0);
        return(0);
    case WM_PAINT:
        D3DVertex vertices[3];

        vertices[0].x = 50;
        vertices[0].y = 50;
        vertices[0].z = 0;
        vertices[0].rhw = 1.0f;
        vertices[0].color = 0x00ff00;

        vertices[1].x = 250;
        vertices[1].y = 50;
        vertices[1].z = 0;
        vertices[1].rhw = 1.0f;
        vertices[1].color = 0x0000ff;

        vertices[2].x = 50;
        vertices[2].y = 250;
        vertices[2].z = 0;
        vertices[2].rhw = 1.0f;
        vertices[2].color = 0xff0000;

        LPDIRECT3DVERTEXBUFFER9 pVertexObject = NULL;
        void *pVertexBuffer = NULL; 

        if(FAILED(g_pDirect3D_Device->CreateVertexBuffer(3*sizeof(D3DVertex), 0, 
            D3DFVF_XYZRHW|D3DFVF_DIFFUSE, D3DPOOL_DEFAULT, &pVertexObject, NULL)))
            return(0);

        if(FAILED(pVertexObject->Lock(0, 3*sizeof(D3DVertex), &pVertexBuffer, 0)))
            return(0);

        memcpy(pVertexBuffer, vertices, 3*sizeof(D3DVertex));
        pVertexObject->Unlock();

        g_pDirect3D_Device->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);

        if(SUCCEEDED(g_pDirect3D_Device->BeginScene()))
        {
         /*   g_pDirect3D_Device->SetStreamSource(0, pVertexObject, 0, sizeof(D3DVertex));
            g_pDirect3D_Device->SetFVF(D3DFVF_XYZRHW|D3DFVF_DIFFUSE);
            g_pDirect3D_Device->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 1);
            g_pDirect3D_Device->EndScene();*/
            g_pDirect3D_Device->SetStreamSource(0, pVertexObject, 0, sizeof(D3DVertex));
            g_pDirect3D_Device->SetFVF(D3DFVF_XYZRHW|D3DFVF_DIFFUSE);
            g_pDirect3D_Device->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 1);
            g_pDirect3D_Device->EndScene();
        }

        g_pDirect3D_Device->Present(NULL, NULL, NULL, NULL);
        ValidateRect(hwnd, NULL);

        g_pDirect3D_Device->Release();
        g_pDirect3D->Release();
        return(0);
    }

    return(DefWindowProc(hwnd, msg, wParam, lParam));
}