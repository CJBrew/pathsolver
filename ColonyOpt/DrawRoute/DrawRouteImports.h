#pragma once

#include "DrawingData.h"

namespace DrawRoute
{
    extern "C" __declspec(dllimport)
        void PostNewData(const DrawingData& dd);
    extern "C" __declspec(dllimport)
        void OpenWindow();
}