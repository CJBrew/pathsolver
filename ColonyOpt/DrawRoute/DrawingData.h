#pragma once

#include <vector>

class DrawingData
{
public:    
    struct Point;
    typedef std::vector<Point> Points;
    typedef Points::const_iterator points_const_iterator;
    typedef Points::iterator points_iterator;

    struct Point    
    {        
        Point(float a, float b):x_(a), y_(b) 
        {}

        float x_;
        float y_;        
    };
 
    void Add(const Point& p);
    void Clear();
    size_t size() const { return points_.size(); }

    Points::const_iterator begin() const { return points_.begin(); }
    Points::const_iterator end() const { return points_.end(); }
    Point at(size_t i) const { return points_.at(i); }
    //Points GetPoints() const { return points_; }
private:
    Points points_;
};
