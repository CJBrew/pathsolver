#pragma once

#include <vector>

typedef double LengthType;
typedef double CoordType;
typedef std::pair<CoordType, CoordType> Coord;
typedef std::vector<Coord> Coords;

//LengthType GetHypotenuse(const Coord& coord1, const Coord& coord2);
bool ComparePathLength(const LengthType a, const LengthType b);
