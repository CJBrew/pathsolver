// PathFinder.cpp : Defines the entry point for the console application.
//
#include "PathObject.h"

#include <map>
#include <set>
#include <string>
#include <iostream>
#include <fstream>
#include <list>
#include <algorithm>
#include <cmath>
#include <ctime>
#include <random>

#pragma warning(push)
#pragma warning(disable:4512) // assignment operator can't be created (boost error?)
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#pragma warning(pop)

using namespace std;
using namespace std::tr1;
using namespace boost::posix_time;
using namespace boost::gregorian;
    
void Test();

const int GENERATIONSIZE = 25;    

void Randomise(PathObject& po, std::tr1::minstd_rand& rand)
{    
    /*
    1
    2
    3  -
    4
    5 -
    6
    7 -
    8
    9 -
    10
    */
    const size_t s = po.GetSize();    
    const size_t pos1 = rand()%s;
    const size_t pos2 = rand()%s;
    
    /*const size_t maxGap = s/4;
    const size_t ranGap = rand() % maxGap;
    size_t pos2 = pos1 + ranGap;
    if(pos2 >= s)
    {
        pos2 -= s;
    }*/
    po.Reverse(pos1, pos2);
}

ostream& operator<<(ostream& os, const Coord& c)
{
    os << "(" << c.first << "," << c.second << ")";
    return os;
}

ostream& operator<<(ostream& os, const Coords& cl)
{
    for(Coords::const_iterator iter = cl.begin(); 
        iter != cl.end();
        ++iter)
    {
        os << *iter << " ";
    }
    os << "\n";
    return os;
}


date DateOfNextTime(const time_duration::hour_type hour, const time_duration::min_type minute)
{       

    ptime now(second_clock::local_time());        
    date targetDate = now.date();
    ptime testTime(now.date(), hours(hour) + minutes(minute));

    if(now > testTime)
    {        
        targetDate += days(1);        
    }

    return targetDate;
}

typedef vector<PathObject> PathObjectGeneration;

void GenerateNewGeneration(PathObjectGeneration& clg, std::tr1::minstd_rand& rand)
{
    // keep the first one with the best score (shortest normally)
    // set all the rest to equal the first.
    int count = 0;
    int base = 0;
    
    for(PathObjectGeneration::iterator iter = clg.begin() + 1;
        iter != clg.end();
        ++iter)
    {
        // copy one
        *iter = *(clg.begin() + base);
        
        if (count < 12)
        {
            // introduce some changes
            Randomise(*iter, rand);
            iter->CalcLength();
        }
        else if (count < 18)
        {
            // introduce some changes
            Randomise(*iter, rand);
            Randomise(*iter, rand);
            Randomise(*iter, rand);
            iter->CalcLength();
        }
        else
        {
            for(int j = 0; j < 10 ; ++j)
            {
                Randomise(*iter, rand);             
            }
            iter->CalcLength();
        }
        ++count;
        ++base;
        if(3 == base) 
        {
            base = 0;
        }
    }

}


PointsMapItem GetRandomUnusedNeighbour(const set<int>* pointsNotUsed)
{
    const int point = rand() % pointsNotUsed.size();

    const int currentIndex = current->first;    
    
    
    for(set<int>::const_iterator iter = pointsNotUsed->begin();
        iter != pointsNotUsed->end();
        ++iter)
    {
        PointDistanceMap::const_iterator it = distances->find(GetPointsPair(currentIndex, *iter));
        if(distances->end() == it)
        {
            const string s("Distance should exist for a point pair!");
            throw s;
        }
        if(it->second < bestLength)
        {
            bestLength = it->second;
            bestNeighbour = *iter;
        }
    }
    if(MAXLENGTH == bestLength || -1 == bestNeighbour)
    {
         const string s("No neighbour found!");
         throw s;
    }
    PointsMap::const_iterator iter = map->find(bestNeighbour);
    return *iter;
}


PointsMapItem GetNearestUnusedNeighbour(const PointsMap* map,
                                        const set<int>* pointsNotUsed,
                                        const PointDistanceMap* distances,
                                        const PointsMapItem* current)
{
    const LengthType MAXLENGTH = 100000;
    const int currentIndex = current->first;    
    LengthType bestLength = MAXLENGTH;
    int bestNeighbour = -1;
    for(set<int>::const_iterator iter = pointsNotUsed->begin();
        iter != pointsNotUsed->end();
        ++iter)
    {
        PointDistanceMap::const_iterator it = distances->find(GetPointsPair(currentIndex, *iter));
        if(distances->end() == it)
        {
            const string s("Distance should exist for a point pair!");
            throw s;
        }
        if(it->second < bestLength)
        {
            bestLength = it->second;
            bestNeighbour = *iter;
        }
    }
    if(MAXLENGTH == bestLength || -1 == bestNeighbour)
    {
         const string s("No neighbour found!");
         throw s;
    }
    PointsMap::const_iterator iter = map->find(bestNeighbour);
    return *iter;
}

PathObject GetBestGuess(const PointsMap& map, const PointDistanceMap& distances)
{   
    PathObject po;
    set<int> pointsNotUsed;
    for(PointsMap::const_iterator iter = map.begin(); iter != map.end(); ++iter)
    {
        pointsNotUsed.insert(iter->first);
    }

    // get a point, add it to the path
    // look for the closest neighbour which isn't already there, add it    
    const int startIndex = 1;
    PointsMap::const_iterator iter = map.find(startIndex);

    if(map.end() == iter)
    {
        const string s("map didn't contain starting point!");
        throw s;
    }

    Coord c = iter->second;    

    PointsMapItem pmi(startIndex, c);
    
    pointsNotUsed.erase(pmi.first);
    po.AddCoord(pmi.second);
    
    while (po.GetSize() < map.size())
    {        
        // pmi = GetNearestUnusedNeighbour(&map, &pointsNotUsed, &distances, &pmi);
        pmi = GetRandomUnusedNeighbour(&map, &pointsNotUsed, &pmi);

        pointsNotUsed.erase(pmi.first);
        po.AddCoord(pmi.second);
    }

    return po;
}

void Grind()
{    
    PointsMap allPoints;
    PointDistanceMap distances;

    GetPaths("C:\\Documents and Settings\\gbyatchr\\My Documents\\CodeFun\\PathFinder\\qatar194.txt", allPoints);

    GetDistances(allPoints, distances);

    std::tr1::minstd_rand randomer;     
    randomer.seed(static_cast<unsigned long>(time(0)));    

    int count = 0;    
    
    PathObject po = GetBestGuess(allPoints, distances);    
    po.CalcLength();
    PathObjectGeneration pog;       
    for(int i = 0 ; i < GENERATIONSIZE ; ++i)
    {
        pog.push_back(po);        
        //pog.insert(po);
    }

    Coords bestList;
    bestList = pog.begin()->GetList();
    LengthType bestLength;
    bestLength = pog.begin()->Length();
    ptime startTime(second_clock::local_time());
    boost::posix_time::time_duration maxSearchTime(0, 2, 0);
    boost::posix_time::time_duration timeused(0,0,0);

    while(timeused < maxSearchTime)
    {
        GenerateNewGeneration(pog, randomer);        
        sort(pog.begin(), pog.end());
        if(ComparePathLength(bestLength, pog.begin()->Length()))
        {
            bestList = pog.begin()->GetList();
            bestLength = pog.begin()->Length();
            cout << "best: " /*<< bestList << " : " */<< pog.begin()->Length() << endl;
        }
        ++count;
        timeused = second_clock::local_time() - startTime;
    }
    cout << "The best: " /*<< pog.begin()->GetList() */
        << " len = " << pog.begin()->Length() << " iterations: " << count << endl;

}

int main(int /*argc*/, char* /*argv[]*/)
{
    try
    {
        Test();
        Grind();
    }
    catch(const string& s)
    {
        cout << " Caught exception: " << s << endl;
    }
    return 0;
}

