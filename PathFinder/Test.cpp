// PathFinder.cpp : Defines the entry point for the console application.
//
#include "PathObject.h"
#include <string>
#include <iostream>
#include <algorithm>
#include <vector>

#include <cmath>
#include <random>

using namespace std;

void Assert(bool b, string s)
{
    if (!b)
    {
        throw s;
    }
}

void Test1()
{
        PathObject po;
        Coord one(0,0);
        Coord two(3,4);
        po.AddCoord(one);
        po.AddCoord(two);
        po.CalcLength();
        Assert(5 == po.Length(), " 3 + 4 -> 5 ");
}

bool DoublEq(const double a, const double b, const double tol = 1.0e-3)
{
    return fabs(a - b) < tol;
}

namespace
{
    class C
    {
    public:
        C(const double d):d_(d)
        {}

        double GetD() const { return d_; }
    private:
        double d_;
    };

bool CompareDoubles(const C a, const C b)
{
    return a.GetD() < b.GetD();
}
}
void Test3()
{
    vector<C> v;
    for(int i = 10; i > 0; --i)
    {
        v.push_back(C(static_cast<double>(i)));
    }
    sort(v.begin(), v.end(), CompareDoubles);
    Assert(v[3].GetD() == 4, "V[3] = 4");
}

void Test2()
{
        Coord one(0,0);
        Coord two(3,4);
        Coord three(10,3);
        Coord four(8,6);
        
        PathObject po;
        po.AddCoord(one);
        po.AddCoord(two);
        po.AddCoord(three);
        po.AddCoord(four);
        po.CalcLength();
        Assert(DoublEq(15.677, po.Length()), " Test2, a ");
        po.Reverse(1,3);
        po.CalcLength();
        Assert(DoublEq(22.896, po.Length()), " Test2, b ");
}



void Test()
{
    Test1();
    Test2();
    Test3();
}
