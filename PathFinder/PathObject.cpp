#include "PathObject.h"
#include <cmath>
#include <fstream>
#include <algorithm>

using namespace std;

PathObject::PathObject():path_(),length_(0)
{
    CalcLength();
}

PathObject::PathObject(const Coords& cl):path_(cl.begin(), cl.end()),length_()
{
    CalcLength();
}

PathObject::PathObject(const PathObject& that):path_(that.GetList()),length_(that.Length())
{
}

PathObject& PathObject::operator=(const PathObject& rhs)
{
    if(&rhs == this)
    {
        return *this;
    }

    path_ = rhs.GetList();
    length_ = rhs.Length();
    return *this;
}

void PathObject::Reverse(size_t index1, size_t index2)
{
    if(index1 > path_.size() - 1 
        || index2 > path_.size() - 1)
    {
        const string s("Invalid index in pathobject::Reverse\n");
        throw s;
    }
    if(index1 > index2)
    {
        swap(index1, index2);
    }
    if(index1 == index2) return;
    Coords::iterator first = path_.begin();
    first += index1;
    Coords::iterator second = path_.begin();
    second += index2;
    reverse(first, second);    
}

bool operator<(const PathObject& lhs, const PathObject& rhs)
{    
    return lhs.Length() < rhs.Length();
}

void PathObject::AddCoord(const Coord& c)
{
    path_.push_back(c);
}

// 2d

namespace
{
    
    LengthType GetHypotenuse(const Coord& coord1, const Coord& coord2)
    {
        LengthType x = static_cast<LengthType>(coord2.first - coord1.first);
        LengthType y = static_cast<LengthType>(coord2.second - coord1.second);
        return static_cast<LengthType>(sqrt(x*x + y*y));
    }
}

void PathObject::CalcLength()
{
    if(path_.size() < 2) return;

    length_ = 0;
    for(Coords::const_iterator iter = path_.begin();
        iter+1 != path_.end();
        ++iter)
    {
        length_ += GetHypotenuse(*iter, *(iter+1));                
    }
}


PointPair GetPointsPair(const int index1, const int index2)
{
    if(index1 == index2)
    {
        const string s("Indexes should be unequal (getpointspair)");
        throw s;
    }
    if(index1 < index2)
    {
            return PointPair(index1, index2);
    }
    else
    {
            return PointPair(index2, index1);
    }
}

void GetDistances(const PointsMap& map, PointDistanceMap& distances)
{
    int index1;
    int index2;

    for(PointsMap::const_iterator iter = map.begin();
            iter != map.end(); ++iter)
    {
        index1 = iter->first;
        for(PointsMap::const_iterator iter2 = iter;
            iter2 != map.end(); ++iter2)
        {
            if(iter2 == iter) continue;

            index2 = iter2->first;

            LengthType len = GetHypotenuse(iter->second, iter2->second);

            distances[PointPair(index1, index2)] = len;
        }
    }
}

void GetPaths(const std::string filename, PointsMap& map)
{
    ifstream ifp(filename.c_str(), ios_base::in);
    
    while(ifp)
    {
        int index;
        Coord temp;
        ifp >> index >> temp.first >> temp.second;
        map.insert(PointsMapItem(index, temp));
    }
}
