#pragma once

#include <map>
#include "Coords.h"

typedef std::pair<int, Coord> PointsMapItem;
typedef std::map<int, Coord> PointsMap;

typedef std::pair<int, int> PointPair;
typedef std::map<std::pair<int, int>, LengthType> PointDistanceMap;

class PathObject
{
public:
    PathObject();
    explicit PathObject(const Coords& cl);
    PathObject(const PathObject&);
    PathObject& operator=(const PathObject&rhs);

    void AddCoord(const Coord&);
    void Reverse(const size_t index1, const size_t index2);
    LengthType Length() const { return length_; }
    void CalcLength();

    typedef Coords::iterator iterator;
    typedef Coords::const_iterator const_iterator;
    iterator begin() { return path_.begin(); }
    iterator end() { return path_.end(); }

    Coords GetList() const { return path_; }
    size_t GetSize() const { return path_.size(); }
private:    
    Coords path_;
    LengthType length_;
};

bool operator<(const PathObject& lhs, const PathObject& rhs);

PointPair GetPointsPair(const int index1, const int index2);
void GetDistances(const PointsMap& map, PointDistanceMap& distances);
void GetPaths(const std::string filename, PointsMap& map);

